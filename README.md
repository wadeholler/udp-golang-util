server and client example and utility in golang

tips for tunneling udp<->tcp with socat
server side  

	socat tcp4-listen:5353,reuseaddr,fork UDP:localhost:10002

client side 
	
	socat -T15 udp4-recvfrom:10003,reuseaddr,fork tcp:localhost:5353

